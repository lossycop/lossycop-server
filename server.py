import argparse
import asyncio
import json
import os
import random
import sys

from aiohttp import web
from lossycop import lossycop

OP_TIMEOUT = 30


class JsonResponse(web.Response):
    def __init__(self, data, compact=True, **kwargs):
        kwargs.setdefault('content_type', 'application/json')
        indent = None if compact else 4
        super().__init__(text=json.dumps(data, indent=indent), **kwargs)


class LossycopServer:
    def __init__(self):
        self.app = web.Application(
            client_max_size=128 * 1024 * 1024,  # 128MB max request size
        )
        self.app.add_routes([
            web.get('/predict', self.predict_get),
            web.post('/predict', self.predict),
        ])
        self.model = lossycop.load_model()
        self.op_lock = asyncio.Lock()

    async def predict_get(self, request):
        return web.Response(
            body='Method not allowed - this resource is available via POST.'
                 ' For more info, check the forum post.'
                 ' If you would like to use it via a UI, please submit that as a feature request.',
            status=405,
        )

    async def predict(self, request):
        temp_file = os.path.join('/tmp', str(random.randint(0, sys.maxsize)))
        try:
            with open(temp_file, 'wb') as f:
                f.write(await request.read())
            try:
                with (await asyncio.wait_for(self.op_lock, timeout=OP_TIMEOUT)):
                    result = lossycop.process_file(self.model, temp_file)
                return JsonResponse(
                    data={'result': result},
                    status=200,
                )
            except asyncio.TimeoutError:
                return JsonResponse(
                    data={'detail': 'Server is overloaded. Please try again later.'},
                    status=503,
                )
        finally:
            pass
            # try:
            #     os.remove(temp_file)
            # except Exception:
            #     pass

    def run(self, port):
        web.run_app(self.app, host='localhost', port=port)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('port', type=int)
    args = parser.parse_args()

    server = LossycopServer()
    server.run(args.port)
